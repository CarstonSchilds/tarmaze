FROM python:3.8.5-alpine

COPY tarmaze.py /bin/tarmaze.py

ENTRYPOINT [ "tarmaze.py" ]
