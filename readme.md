# tarmaze

`tarmaze` is a CLI for building and solving _mazes_ that are composed of nested TAR files. A secret file or string can be stored at the solution of the _maze_.

## Usage
`tarmaze` has two modes of operation, the `build` command can be used to build the TAR file maze, and the `solve` command can be used to solve the maze. Use the `-h` flag to describe the breadth of options available for each command.
### Examples
```sh
./tarmaze.py build 4 4 # builds a tarmaze of depth 4 with 4 branches at each layer
./tarmaze.py build 4 4 -f my-flag.txt # hides the file my-flag.txt in the maze at the solution
./tarmaze.py solve .tarmaze/_A.tar.gz # solves the tarmaze _A.tar.gz
./tarmaze.py solve .tarmaze/_A.tar.gz -r # reads the contents of the secret file hidden at the solution
```

### Docker Usage
```sh
alias tarmaze="docker run --rm -v /tmp/tarmaze:/tmp/tarmaze registry.gitlab.com/carstonschilds/tarmaze:latest -t=/tmp/tarmaze"
# note: only the /tmp/tarmaze directory is accessible to the container
# otherwise, this should work exactly like using the normal script
tarmaze build 4 4
tarmaze solve -r /tmp/tarmaze/_A.tar.gz
```

## Development

### Setup
```sh
git clone git@gitlab.com:CarstonSchilds/tarmaze.git
python3 -m venv venv
. venv/bin/activate
pip3 install requirements.txt
python3 ./tarmaze.py
```

### Static Analysis
You can run all static required static analysis locally with:
```sh
flake8 --statistics --max-line-length=120 tarmaze.py
mypy tarmaze.py
pylint --max-line-length=120 tarmaze.py
docker run --rm -i hadolint/hadolint:latest-alpine < Dockerfile
```

### Updating Dependencies
```sh
pip3 freeze > requirements.txt
python3 -m pur -r requirements.txt
```